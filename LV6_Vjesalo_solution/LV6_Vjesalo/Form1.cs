﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6_Vjesalo
{
    public partial class Vjesalo : Form
    {
        static string put;

        string datoteka;
        string target;
        int i = 0;
        char guess;
        int tries;
        bool hit = false;

        public Vjesalo()
        {
            InitializeComponent();
        }

        private void BiraRijec_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
                put = ofd.FileName;
            textBox1.Text = put;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

       

        private void Pogodi_Click(object sender, EventArgs e)
        {
            guess = Slovo.Text[0];
            for (i = 0; i < target.Length; i++)
            {
                if (Char.ToUpper(target[i]) == Char.ToUpper(guess))
                {
                    hit = true;
                    label1.Text = label1.Text.Remove(i, 1);
                    label1.Text = label1.Text.Insert(i, guess.ToString());
                }
            }
            if (label1.Text.ToUpper() == target.ToUpper())
                WonGame();

            if (!hit)
            {
                tries++;
                int tries_left = 9;
                label2.Text = (tries_left - tries).ToString();
                if (tries == 9)
                    LostGame();

            }
            hit = false;
            Slovo.Focus();
            Slovo.SelectAll();
        }


        private void LostGame()
        {
            MessageBox.Show("YOU LOST! Secet word was" + target);
            ResetGame();
        }
        private void WonGame()
        {
            MessageBox.Show("YOU WON!Secet word was" + target);
            ResetGame();
        }
        private void ResetGame()
        {
            Slovo.Text = "";
            label1.Text = "Secret word";
            target = "";
        }



        private void Setting()
        {
            label1.Text = "";
            for (i = 0; i < target.Length; i++)
                label1.Text = label1.Text.Insert(i, "*");

        }

        private void Start_Click(object sender, EventArgs e)
        {
            datoteka = put;
            StreamReader sr = new StreamReader(datoteka);
            while (sr.ReadLine() != null)
                i++;
            sr.Dispose();
            sr = new StreamReader(datoteka);
            Random r = new Random();
            for (int k = 20; k < r.Next(1, i - 1); k++)
                target = sr.ReadLine();
            sr.Dispose();
            MessageBox.Show(target);
            Setting();
        }
    }
}
