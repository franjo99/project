﻿namespace LV6_Vjesalo
{
    partial class Vjesalo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BiraRijec = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.pogodi_slovo = new System.Windows.Forms.Label();
            this.Slovo = new System.Windows.Forms.TextBox();
            this.Pogodi = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Start = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BiraRijec
            // 
            this.BiraRijec.Location = new System.Drawing.Point(30, 21);
            this.BiraRijec.Name = "BiraRijec";
            this.BiraRijec.Size = new System.Drawing.Size(138, 49);
            this.BiraRijec.TabIndex = 0;
            this.BiraRijec.Text = "Rijec";
            this.BiraRijec.UseVisualStyleBackColor = true;
            this.BiraRijec.Click += new System.EventHandler(this.BiraRijec_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(186, 34);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(246, 22);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // pogodi_slovo
            // 
            this.pogodi_slovo.AutoSize = true;
            this.pogodi_slovo.Location = new System.Drawing.Point(34, 140);
            this.pogodi_slovo.Name = "pogodi_slovo";
            this.pogodi_slovo.Size = new System.Drawing.Size(93, 17);
            this.pogodi_slovo.TabIndex = 3;
            this.pogodi_slovo.Text = "Pogodi slovo:";
            // 
            // Slovo
            // 
            this.Slovo.Location = new System.Drawing.Point(133, 137);
            this.Slovo.MaxLength = 1;
            this.Slovo.Name = "Slovo";
            this.Slovo.Size = new System.Drawing.Size(100, 22);
            this.Slovo.TabIndex = 4;
            // 
            // Pogodi
            // 
            this.Pogodi.Location = new System.Drawing.Point(264, 124);
            this.Pogodi.Name = "Pogodi";
            this.Pogodi.Size = new System.Drawing.Size(78, 49);
            this.Pogodi.TabIndex = 5;
            this.Pogodi.Text = "Pogodi";
            this.Pogodi.UseVisualStyleBackColor = true;
            this.Pogodi.Click += new System.EventHandler(this.Pogodi_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(140, 262);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 29);
            this.label1.TabIndex = 6;
            this.label1.Text = "label1";
            // 
            // Start
            // 
            this.Start.Location = new System.Drawing.Point(456, 21);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(55, 49);
            this.Start.TabIndex = 7;
            this.Start.Text = "Start";
            this.Start.UseVisualStyleBackColor = true;
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(467, 261);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 46);
            this.label2.TabIndex = 8;
            this.label2.Text = "label2";
            // 
            // Vjesalo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Start);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Pogodi);
            this.Controls.Add(this.Slovo);
            this.Controls.Add(this.pogodi_slovo);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.BiraRijec);
            this.Name = "Vjesalo";
            this.Text = "Vjesalo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BiraRijec;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label pogodi_slovo;
        private System.Windows.Forms.TextBox Slovo;
        private System.Windows.Forms.Button Pogodi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Start;
        private System.Windows.Forms.Label label2;
    }
}

