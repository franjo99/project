﻿namespace LV6
{
    partial class Znanst_kalk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Broj1 = new System.Windows.Forms.TextBox();
            this.puta = new System.Windows.Forms.Button();
            this.Broj2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Rezultat = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.podjeljeno = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.sinus = new System.Windows.Forms.Button();
            this.kosinus = new System.Windows.Forms.Button();
            this.prirodni_log = new System.Windows.Forms.Button();
            this.korijen = new System.Windows.Forms.Button();
            this.eksponent = new System.Windows.Forms.Button();
            this.e_Exp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Broj1
            // 
            this.Broj1.Location = new System.Drawing.Point(57, 43);
            this.Broj1.Name = "Broj1";
            this.Broj1.Size = new System.Drawing.Size(94, 22);
            this.Broj1.TabIndex = 0;
            // 
            // puta
            // 
            this.puta.Font = new System.Drawing.Font("Verdana", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.puta.Location = new System.Drawing.Point(255, 41);
            this.puta.Name = "puta";
            this.puta.Size = new System.Drawing.Size(73, 45);
            this.puta.TabIndex = 1;
            this.puta.Text = "*";
            this.puta.UseVisualStyleBackColor = true;
            this.puta.Click += new System.EventHandler(this.puta_Click);
            // 
            // Broj2
            // 
            this.Broj2.Location = new System.Drawing.Point(57, 71);
            this.Broj2.Name = "Broj2";
            this.Broj2.Size = new System.Drawing.Size(94, 22);
            this.Broj2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Br1:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Br2:";
            // 
            // Rezultat
            // 
            this.Rezultat.BackColor = System.Drawing.SystemColors.MenuBar;
            this.Rezultat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Rezultat.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rezultat.Location = new System.Drawing.Point(16, 159);
            this.Rezultat.Name = "Rezultat";
            this.Rezultat.Size = new System.Drawing.Size(173, 21);
            this.Rezultat.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Rezultat:";
            // 
            // podjeljeno
            // 
            this.podjeljeno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.podjeljeno.Location = new System.Drawing.Point(334, 39);
            this.podjeljeno.Name = "podjeljeno";
            this.podjeljeno.Size = new System.Drawing.Size(73, 45);
            this.podjeljeno.TabIndex = 7;
            this.podjeljeno.Text = "/";
            this.podjeljeno.UseVisualStyleBackColor = true;
            this.podjeljeno.Click += new System.EventHandler(this.podjeljeno_Click);
            // 
            // plus
            // 
            this.plus.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plus.Location = new System.Drawing.Point(255, 90);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(73, 45);
            this.plus.TabIndex = 8;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // minus
            // 
            this.minus.Font = new System.Drawing.Font("Verdana", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minus.Location = new System.Drawing.Point(334, 90);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(73, 45);
            this.minus.TabIndex = 9;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // sinus
            // 
            this.sinus.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sinus.Location = new System.Drawing.Point(255, 141);
            this.sinus.Name = "sinus";
            this.sinus.Size = new System.Drawing.Size(73, 45);
            this.sinus.TabIndex = 10;
            this.sinus.Text = "sin";
            this.sinus.UseVisualStyleBackColor = true;
            this.sinus.Click += new System.EventHandler(this.sinus_Click);
            // 
            // kosinus
            // 
            this.kosinus.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kosinus.Location = new System.Drawing.Point(334, 141);
            this.kosinus.Name = "kosinus";
            this.kosinus.Size = new System.Drawing.Size(73, 45);
            this.kosinus.TabIndex = 11;
            this.kosinus.Text = "cos";
            this.kosinus.UseVisualStyleBackColor = true;
            this.kosinus.Click += new System.EventHandler(this.kosinus_Click);
            // 
            // prirodni_log
            // 
            this.prirodni_log.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prirodni_log.Location = new System.Drawing.Point(255, 192);
            this.prirodni_log.Name = "prirodni_log";
            this.prirodni_log.Size = new System.Drawing.Size(73, 45);
            this.prirodni_log.TabIndex = 12;
            this.prirodni_log.Text = "ln";
            this.prirodni_log.UseVisualStyleBackColor = true;
            this.prirodni_log.Click += new System.EventHandler(this.prirodni_log_Click);
            // 
            // korijen
            // 
            this.korijen.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.korijen.Location = new System.Drawing.Point(334, 192);
            this.korijen.Name = "korijen";
            this.korijen.Size = new System.Drawing.Size(73, 45);
            this.korijen.TabIndex = 13;
            this.korijen.Text = "sqrt";
            this.korijen.UseVisualStyleBackColor = true;
            this.korijen.Click += new System.EventHandler(this.korijen_Click);
            // 
            // eksponent
            // 
            this.eksponent.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eksponent.Location = new System.Drawing.Point(334, 243);
            this.eksponent.Name = "eksponent";
            this.eksponent.Size = new System.Drawing.Size(73, 45);
            this.eksponent.TabIndex = 14;
            this.eksponent.Text = "^Br2";
            this.eksponent.UseVisualStyleBackColor = true;
            this.eksponent.Click += new System.EventHandler(this.eksponent_Click);
            // 
            // e_Exp
            // 
            this.e_Exp.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_Exp.Location = new System.Drawing.Point(255, 243);
            this.e_Exp.Name = "e_Exp";
            this.e_Exp.Size = new System.Drawing.Size(73, 45);
            this.e_Exp.TabIndex = 15;
            this.e_Exp.Text = "e^Br1";
            this.e_Exp.UseVisualStyleBackColor = true;
            this.e_Exp.Click += new System.EventHandler(this.e_Exp_Click);
            // 
            // Znanst_kalk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 528);
            this.Controls.Add(this.e_Exp);
            this.Controls.Add(this.eksponent);
            this.Controls.Add(this.korijen);
            this.Controls.Add(this.prirodni_log);
            this.Controls.Add(this.kosinus);
            this.Controls.Add(this.sinus);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.podjeljeno);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Rezultat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Broj2);
            this.Controls.Add(this.puta);
            this.Controls.Add(this.Broj1);
            this.Name = "Znanst_kalk";
            this.Text = "Znanstveni kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Broj1;
        private System.Windows.Forms.Button puta;
        private System.Windows.Forms.TextBox Broj2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Rezultat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button podjeljeno;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button sinus;
        private System.Windows.Forms.Button kosinus;
        private System.Windows.Forms.Button prirodni_log;
        private System.Windows.Forms.Button korijen;
        private System.Windows.Forms.Button eksponent;
        private System.Windows.Forms.Button e_Exp;
    }
}

